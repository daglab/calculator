<?php

namespace Ucheniki\Calculator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class CalculateController extends Controller
{
    public function add($a,$b){
        $result = $a + $b;
        return view('calculator::calc',compact('result'));
    }
    public function sub($a,$b){
        echo $a - $b;
    }
}
